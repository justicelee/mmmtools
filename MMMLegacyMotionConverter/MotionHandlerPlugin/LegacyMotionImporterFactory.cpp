#include "LegacyMotionImporterFactory.h"
#include "LegacyMotionImporter.h"

#include <boost/extension/extension.hpp>

using namespace MMM;

// register this factory
MotionHandlerFactory::SubClassRegistry LegacyMotionImporterFactory::registry(LegacyMotionImporter::NAME, &LegacyMotionImporterFactory::createInstance);

LegacyMotionImporterFactory::LegacyMotionImporterFactory() : MotionHandlerFactory() {}

LegacyMotionImporterFactory::~LegacyMotionImporterFactory() {}

std::string LegacyMotionImporterFactory::getName()
{
    return LegacyMotionImporter::NAME;
}

MotionHandlerPtr LegacyMotionImporterFactory::createMotionHandler(QWidget* widget) {
    return MotionHandlerPtr(new LegacyMotionImporter(widget));
}

MotionHandlerFactoryPtr LegacyMotionImporterFactory::createInstance(void *)
{
    return MotionHandlerFactoryPtr(new LegacyMotionImporterFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL MotionHandlerFactoryPtr getFactory() {
    return MotionHandlerFactoryPtr(new LegacyMotionImporterFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return MotionHandlerFactory::VERSION;
}
