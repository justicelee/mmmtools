#include "LegacyMotionImporterDialog.h"
#include "ui_LegacyMotionImporterDialog.h"

#include <QCheckBox>
#include <QListWidget>
#include <QMessageBox>
#include <set>

LegacyMotionImporterDialog::LegacyMotionImporterDialog(MMM::LegacyMotionConverterPtr converter, QWidget* parent) :
    QDialog(parent),
    ui(new Ui::LegacyMotionImporterDialog),
    converter(converter)
{
    ui->setupUi(this);

    motionList = new MotionListWidget(converter->getMotionNames());

    ui->motionListLayout->addWidget(motionList);

    connect(ui->importMotionButton, SIGNAL(clicked()), this, SLOT(importMotion()));
    connect(ui->cancelButton, SIGNAL(clicked()), this, SLOT(close()));
}

LegacyMotionImporterDialog::~LegacyMotionImporterDialog() {
    delete ui;
}

void LegacyMotionImporterDialog::importMotion() {

    try {
        motions = converter->convertMotions(motionList->getConfigurations());
        if (motions.size() > 0) {
            this->close();
        } else {
            QMessageBox* msgBox = new QMessageBox(this);
            msgBox->setText(QString("Please enable minimum one motion!"));
            msgBox->exec();
        }
    } catch (MMM::Exception::MMMException e) {
        QMessageBox* msgBox = new QMessageBox(this);
        msgBox->setText(QString::fromStdString(e.what()));
        msgBox->exec();
    }
}

MMM::MotionList LegacyMotionImporterDialog::getMotions() {
    exec();
    return motions;
}
