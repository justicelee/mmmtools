/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_LEGACYMOTIONCONVERTERCONFIGURATION_H_
#define __MMM_LEGACYMOTIONCONVERTERCONFIGURATION_H_

#include <boost/filesystem.hpp>
#include <string>
#include <VirtualRobot/RuntimeEnvironment.h>
#include <MMM/MMMCore.h>
#include <MMM/XMLTools.h>

#include "../common/ApplicationBaseConfiguration.h"

/*!
    Configuration of MMMLegacyMotionConverter.
*/
class MMMLegacyMotionConverterConfiguration : public ApplicationBaseConfiguration
{
public:
    MMMLegacyMotionConverterConfiguration() : ApplicationBaseConfiguration()
    {
    }

    bool processCommandLine(int argc, char *argv[])
    {
        VirtualRobot::RuntimeEnvironment::considerKey("inputMotion");
        VirtualRobot::RuntimeEnvironment::considerKey("outputMotion");
        VirtualRobot::RuntimeEnvironment::processCommandLine(argc,argv);
        VirtualRobot::RuntimeEnvironment::print();

        inputMotionPath = getParameter("inputMotion", true, true);
        outputMotionPath = getParameter("outputMotion", false, false);
        if (outputMotionPath.empty())
            outputMotionPath = MMM::XML::getFileNameWithoutExtension(inputMotionPath) + "_v2.xml";


        return valid;
    }

    void print()
    {
        MMM_INFO << "*** MMMLegacyMotionsConverter Configuration ***" << std::endl;
        std::cout << "Input motion: " << inputMotionPath << std::endl;
        std::cout << "Output motion: " << outputMotionPath << std::endl;
    }

    std::string inputMotionPath;
    std::string outputMotionPath;
};


#endif // __MMM_LEGACYMOTIONCONVERTERCONFIGURATION_H_
