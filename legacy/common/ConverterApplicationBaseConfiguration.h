/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMMTools
* @copyright  2015 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __ConverterApplicationBaseConfiguration_H_
#define __ConverterApplicationBaseConfiguration_H_

#include <string>

class ConverterApplicationBaseConfiguration
{
public:
    enum FileType {
        C3D,
        MMM,
        UNKNOWN
    };

    ConverterApplicationBaseConfiguration();

    //! checks for command line parameters and updates configuration accordingly.
    virtual bool processCommandLine(int argc, char *argv[]);

    virtual void print();

    std::vector<std::string> converterLibSearchPaths;

    std::string converterName;
    std::string converterConfigFile;

    std::string inputFile;
    std::string markerPrefix;

    std::string sourceModelFile;
    std::string sourceModelProcessor;
    std::string sourceModelProcessorConfigFile;

    std::string targetModelFile;
    std::string targetModelProcessor;
    std::string targetModelProcessorConfigFile;

    FileType inputFileType;

protected:
    bool valid;

    std::string getParameter(std::string parameterName, bool required, bool isPath);
};

#endif
