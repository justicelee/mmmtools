
#include "ConverterVicon2MMMFactory.h"
#include "ConverterVicon2MMM.h"
#include <boost/extension/shared_library.hpp>
#include <boost/function.hpp>

namespace MMM
{

// register this factory
ConverterFactory::SubClassRegistry ConverterVicon2MMMFactory::registry(ConverterVicon2MMMFactory::getName(), &ConverterVicon2MMMFactory::createInstance);

ConverterVicon2MMMFactory::ConverterVicon2MMMFactory()
	: ConverterFactory()
{

}
ConverterVicon2MMMFactory::~ConverterVicon2MMMFactory()
{

}

ConverterPtr ConverterVicon2MMMFactory::createConverter()
{
	ConverterPtr converter(new ConverterVicon2MMM(getName()));
	return converter;
}

std::string ConverterVicon2MMMFactory::getName()
{
	return "ConverterVicon2MMM";
}

boost::shared_ptr<ConverterFactory> ConverterVicon2MMMFactory::createInstance(void*)
{
	boost::shared_ptr<ConverterFactory> converterFactory(new ConverterVicon2MMMFactory());
	return converterFactory;
}

}
#ifdef WIN32
#pragma warning(push)
#pragma warning(disable: 4190) // C-linkage warning can be ignored in our case
#endif

extern "C"
BOOST_EXTENSION_EXPORT_DECL MMM::ConverterFactoryPtr getFactory() {
    MMM::ConverterVicon2MMMFactoryPtr f(new MMM::ConverterVicon2MMMFactory());
    return f;
}
#ifdef WIN32
#pragma warning(pop)
#endif
