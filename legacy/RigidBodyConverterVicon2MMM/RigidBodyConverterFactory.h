/**
* This file is part of MMM.
*
* MMM is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/


#ifndef MMM_RIGIDBODYCONVERTERFACTORY_H
#define MMM_RIGIDBODYCONVERTERFACTORY_H

#include <MMM/AbstractFactoryMethod.h>
#include <MMM/MMMCore.h>
#include <MMM/MMMImportExport.h>
#include <MMM/Motion/Legacy/Converter/ConverterFactory.h>

#include "RigidBodyConverterVicon2MMMImportExport.h"

namespace MMM {

    class RigidBodyConverterVicon2MMM_IMPORT_EXPORT RigidBodyConverterFactory :
            public ConverterFactory
    {
    public:
        RigidBodyConverterFactory();



        virtual ~RigidBodyConverterFactory();

        ConverterPtr createConverter();

        static std::string getName();

        static boost::shared_ptr<ConverterFactory> createInstance(void*);

    private:
        static SubClassRegistry registry;
    };

    typedef boost::shared_ptr<RigidBodyConverterFactory> RigidBodyConverterFactoryPtr;


}

#endif
