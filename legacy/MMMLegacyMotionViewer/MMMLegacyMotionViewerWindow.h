#ifndef __MMMLegacyMotionViewer_WINDOW_H_
#define __MMMLegacyMotionViewer_WINDOW_H_

#include "ui_MMMLegacyMotionViewer.h"

#include <VirtualRobot/VirtualRobotCommon.h>
#include <string>
#include <Inventor/sensors/SoSensor.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoSwitch.h>

#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>


#ifndef Q_MOC_RUN
#endif
#include <MMM/MMMCore.h>
#include <MMM/Motion/Legacy/LegacyMotionReaderC3D.h>

// workaround for qt moc, causing problems with boost under visual studio
#ifndef Q_MOC_RUN
#include <MMM/Motion/Legacy/LegacyMotion.h>
#include <MMM/Motion/Legacy/LegacyMotionReaderXML.h>
#include <MMM/Model/ModelProcessorFactory.h>
#include <MMM/Model/ModelProcessor.h>
#endif

//qtable
#include <QStandardItem>
#include <QListWidgetItem>
#include <qgl.h>

class MMMLegacyMotionViewerWindow : public QMainWindow
{
    Q_OBJECT
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    MMMLegacyMotionViewerWindow(const std::string &dataFile, const std::string &markerMotionFile);

    ~MMMLegacyMotionViewerWindow();

	/*!< Executes the SoQt mainLoop. You need to call this in order to execute the application. */
	int main();
	void progress();

public slots:

	void quit();
	void redraw();
	void closeEvent(QCloseEvent *event);
	void sliderMoved(int pos);
	void updateVisu();
    void fpsChanged(double newValue);
    void setAntiAliasing(int checkBoxState);
	

protected:
	void setupUI();
    void buildMarkerVisu();
    void updateMarkerVisu(int frame);

	void load();

	//qtable stuff
	void setupTable();
	void updateTable();
	bool checkStateInTable(int row);
	void lockSelectedJointValuesFromTable(const Eigen::VectorXf pose);


	static void timerCB(void * data, SoSensor * sensor);
	void createMotionVisu(VirtualRobot::TrajectoryPtr tr, SoSeparator* addToThisSep);

	void jumpToFrame(int pos);

	// filenames
    std::string dataFile;
    std::string markerMotionFile;
	std::string modelFile;
	// Motion and Poses
    MMM::LegacyMotionList motions;
    MMM::MarkerMotionPtr markerMotion;


	// organizing timer
	SoSensorManager* _pSensorMgr;
	SoTimerSensor* _pSensorTimer;
	int _nLastPosition;

    std::map<std::string, VirtualRobot::RobotNodeSetPtr> robotNodeSets;

	MMM::ModelProcessorFactoryPtr modelFactory;

	// Robot Pointer and Seperator Nodes
    std::map<std::string, VirtualRobot::RobotPtr> robots;
    std::map<std::string, SoSeparator*> robotSeps;
    SoSeparator *sceneSep;
    SoSeparator *markerSep;
    SoSeparator *motionSep;
	SoSeparator *rootCoordSep;
	SoSeparator *floorSep;
    SoSwitch *swFloor;

	// UI Class and Viewer
    Ui::MainWindowMMMLegacyMotionViewer UI;
	SoQtExaminerViewer *viewer;
	bool _bShowValues;
	bool timerSensorAdded;
    void saveScreenshot();

private slots:
	void on_tableWidget_cellClicked(int row, int column);

	void on_playButton_clicked();
	void on_stopButton_clicked();
    void saveMotion();
};

#endif 
