#ifndef __MMMDynamicsCalculatorConfiguration_H_
#define __MMMDynamicsCalculatorConfiguration_H_

#include <boost/filesystem.hpp>
#include <string>
#include <VirtualRobot/RuntimeEnvironment.h>

/*!
    Configuration of MMM dynamics calculator.
*/
struct MMMDynamicsCalculatorConfiguration
{
    MMMDynamicsCalculatorConfiguration()
    {
        inputMotionPath = std::string(MMMDYNAMICSCALCULATOR_BASE_DIR) + std::string("/../data/Motions/WalkingStraightForward07.xml");
        outputMotionPath = "motion_with_dynamics.xml";
    }

    bool processCommandLine(int argc, char *argv[])
    {
        VirtualRobot::RuntimeEnvironment::considerKey("inputMotion");
        VirtualRobot::RuntimeEnvironment::considerKey("outputMotion");
        VirtualRobot::RuntimeEnvironment::processCommandLine(argc,argv);
        VirtualRobot::RuntimeEnvironment::print();

        VirtualRobot::RuntimeEnvironment::addDataPath(MMMDYNAMICSCALCULATOR_BASE_DIR);

        if (VirtualRobot::RuntimeEnvironment::hasValue("inputMotion"))
            inputMotionPath = VirtualRobot::RuntimeEnvironment::getValue("inputMotion");

        if (VirtualRobot::RuntimeEnvironment::hasValue("outputMotion"))
            outputMotionPath = VirtualRobot::RuntimeEnvironment::getValue("outputMotion");

        if (!VirtualRobot::RuntimeEnvironment::getDataFileAbsolute(inputMotionPath))
        {
            MMM_ERROR << "Could not find input motion file " << inputMotionPath << "!" << std::endl;
            return false;
        }

        return true;
    }

    void print()
    {
        MMM_INFO << "*** MMMDynamicsCalculator Configuration ***" << std::endl;
        std::cout << "Input motion: " << inputMotionPath << std::endl;
        std::cout << "Output motion: " << outputMotionPath << std::endl;
    }

    std::string inputMotionPath;
    std::string outputMotionPath;
};

#endif
