/*
 * This file is part of MMM.
 *
 * MMM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MMM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MMM.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MMM
 * @author     Andre Meixner <andre.meixner@student.kit.edu>
 * @copyright  2015 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
 *
 */

#include <VirtualRobot/RuntimeEnvironment.h>
#include <MMM/RapidXML/RapidXMLReader.h>
#include <MMM/Motion/Motion.h>
#include <MMM/Motion/MotionReaderXML.h>
#include <MMM/Motion/MotionWriterXML.h>
#include <MMM/Motion/Plugin/IMUPlugin/IMUSensor.h>
#include <MMM/Motion/Plugin/IMUPlugin/IMUSensorMeasurement.h>
#include <MMM/FactoryPluginLoader.h>
#include "MMMIMUConverterConfiguration.h"
#include "IMUConverter.h"
#include "IMUConverterFactory.h"

#include <string>
#include <map>
#include <vector>
#include <exception>

MMM::IMUSensorPtr readIMUConfigurationFile(const std::string &inputIMUConfigurationFilePath) {
    MMM_INFO << "Loading IMU Configuration file '" << inputIMUConfigurationFilePath << "'!" << std::endl;
    MMM::RapidXMLReaderPtr reader = MMM::RapidXMLReader::FromFile(inputIMUConfigurationFilePath);
    MMM::RapidXMLReaderNodePtr root = reader->getRoot();
    return MMM::IMUSensor::loadSensorConfigurationXML(root);
}

int main(int argc, char *argv[])
{
    MMM_INFO << " --- MMMIMUConverter --- " << std::endl;
    MMMIMUConverterConfiguration *configuration = new MMMIMUConverterConfiguration();
    if (!configuration->processCommandLine(argc, argv)) {
        MMM_ERROR << "Error while processing command line, aborting..." << std::endl;
        return -1;
    }

    MMM::MotionList motions;
    MMM::MotionPtr  motion;
    MMM::IMUSensorPtr imuSensor;
    try {
        MMM_INFO << "Reading motion file '" << configuration->inputMotionPath << "'!" << std::endl;
        MMM::MotionReaderXMLPtr motionReader(new MMM::MotionReaderXML(configuration->sensorPluginPaths));
        motions  = motionReader->loadAllMotions(configuration->inputMotionPath);
        motion = MMM::Motion::getMotion(motions, configuration->motionName);
        imuSensor = readIMUConfigurationFile(configuration->imuConfigurationFilePath);
    }
    catch (MMM::Exception::MMMException &e) {
        MMM_ERROR << e.what() << std::endl;
        return -2;
    }

    imuSensor->setName(configuration->sensorName);
    imuSensor->setDescription(configuration->sensorDescription);

    boost::shared_ptr<MMM::FactoryPluginLoader<MMM::IMUConverterFactory> > factoryPluginLoader(new MMM::FactoryPluginLoader<MMM::IMUConverterFactory>(configuration->converterPlugins));
    MMM::IMUConverterFactoryPtr converterFactory = factoryPluginLoader->loadFactory(configuration->converter);
    if (!converterFactory) {
        MMM_ERROR << "No imu converter of type " << configuration->converter << " found!" << std::endl;
        return -3;
    }
    MMM::IMUConverterPtr converter = converterFactory->createIMUConverter();
    std::vector<MMM::IMUDataPtr> imuData = converter->convertIMUData(configuration->imuFilePath);
    for (MMM::IMUDataPtr data : imuData) {
        imuSensor->addSensorMeasurement(MMM::IMUSensorMeasurementPtr(new MMM::IMUSensorMeasurement(data->timestep, data->acceleration)));
    }

    try {
        motion->addSensor(imuSensor, configuration->offset);
    }
    catch(MMM::Exception::MMMException e) {
        MMM_ERROR << "Couldn't add acceleration (IMU) sensor to motion! " << e.what() << std::endl;
        return -4;
    }

    MMM_INFO << "Writing motions to " << configuration->outputMotionPath << std::endl;
    MMM::MotionWriterXML::writeMotion(motions, configuration->outputMotionPath);

    return 0;
}
