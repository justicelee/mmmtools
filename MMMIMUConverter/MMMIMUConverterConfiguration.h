/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_IMUCONVERTERCONFIGURATION_H_
#define __MMM_IMUCONVERTERCONFIGURATION_H_

#include "../common/ApplicationBaseConfiguration.h"

#include <boost/filesystem.hpp>
#include <string>
#include <VirtualRobot/RuntimeEnvironment.h>

/*!
    Configuration of MMM dynamics calculator.
*/
class MMMIMUConverterConfiguration : public ApplicationBaseConfiguration
{
public:
    MMMIMUConverterConfiguration() : ApplicationBaseConfiguration(), offset(0.0f)
    {
    }

    std::string inputMotionPath;
    std::string motionName;
    std::string imuFilePath;
    std::string imuConfigurationFilePath;
    std::string converter;
    std::string sensorName;
    std::string sensorDescription;
    std::vector<std::string> converterPlugins;
    std::vector<std::string> sensorPluginPaths;
    std::string outputMotionPath;
    float offset; // for Synchronization

    bool processCommandLine(int argc, char *argv[])
    {
        VirtualRobot::RuntimeEnvironment::considerKey("inputMotion");
        VirtualRobot::RuntimeEnvironment::considerKey("motionName");
        VirtualRobot::RuntimeEnvironment::considerKey("imuFile");
        VirtualRobot::RuntimeEnvironment::considerKey("imuConfigurationFile");
        VirtualRobot::RuntimeEnvironment::considerKey("converter");
        VirtualRobot::RuntimeEnvironment::considerKey("sensorName");
        VirtualRobot::RuntimeEnvironment::considerKey("sensorDescription");
        VirtualRobot::RuntimeEnvironment::considerKey("converterPlugins");
        VirtualRobot::RuntimeEnvironment::considerKey("sensorPlugins");
        VirtualRobot::RuntimeEnvironment::considerKey("outputMotion");
        VirtualRobot::RuntimeEnvironment::considerKey("offset");
        VirtualRobot::RuntimeEnvironment::processCommandLine(argc,argv);
        VirtualRobot::RuntimeEnvironment::print();

        VirtualRobot::RuntimeEnvironment::addDataPath(MMMIMU_CONVERTER_BASE_DIR);

        inputMotionPath = getParameter("inputMotion", true, true);
        motionName = getParameter("motionName", false, false);
        imuFilePath = getParameter("imuFile", true, true);
        imuConfigurationFilePath = getParameter("imuConfigurationFile", true, true);
        converter = getParameter("converter", true, false);
        sensorName = getParameter("sensorName", false, false);
        sensorDescription = getParameter("sensorDescription", false, false);

        converterPlugins = getParameters("imuConverterPlugins", std::string(IMU_PLUGIN_LIB_DIR));

        sensorPluginPaths = getParameters("sensorPlugins");

        outputMotionPath = getParameter("outputMotion", false, false);

        std::string offsetString = getParameter("offset", false, false);
        if (!offsetString.empty()) offset = ::atof(offsetString.c_str());

        return valid;
    }

    void print()
    {
        MMM_INFO << "*** MMMIMUConverter Configuration ***" << std::endl;
        std::cout << "Input motion: " << inputMotionPath << std::endl;
        std::cout << "Motion name: " << motionName << std::endl;
        std::cout << "IMU file:" << imuFilePath << std::endl;
        std::cout << "IMU configuration file:" << imuConfigurationFilePath << std::endl;
        std::cout << "IMU converter:" << converter << std::endl;
        std::cout << "Output motion: " << outputMotionPath << std::endl;
    }
};

#endif
