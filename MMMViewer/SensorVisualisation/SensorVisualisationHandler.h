/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_SENSORVISUALISATIONHANDLER_H_
#define __MMM_SENSORVISUALISATIONHANDLER_H_

#ifndef Q_MOC_RUN
#include <MMM/Motion/Motion.h>
#include "SensorVisualisation.h"
#include "SensorVisualisationFactory.h"
#include "SensorVisualisationTable/SensorVisualisationTable.h"
#endif

#include <QTreeWidget>
#include <QObject>
#include <Inventor/nodes/SoSeparator.h>

#include <set>

class SensorVisualisationHandler : public QObject
{
    Q_OBJECT

public:
    SensorVisualisationHandler(SoSeparator* sceneGraph, bool interpolationEnabled = false, float timestepDelta = 0.01f);

    void loadSensorVisualisation(MMM::MotionList motions);

    bool changeTimestep(float timestep);

    void setInterpolation(bool enabled);

    void setTimestepDelta(float delta);

    QTreeWidget* getSensorVisualisationTable();

    void displaySensorVisualisationTable(bool display);

public slots:
    void updateSensorVisualisation(const std::map<std::string, boost::shared_ptr<MMM::SensorVisualisationFactory> > &sensorVisualisationFactories);

signals:
    void sensorVisualisationLoaded(bool loaded);

private:
    std::map<std::string, std::map<std::string, MMM::SensorVisualisationPtr> > sensorVisualisations;
    SoSeparator* sensorVisualisationSeperator;
    bool interpolationEnabled;
    float timestepDelta;
    std::map<std::string, boost::shared_ptr<MMM::SensorVisualisationFactory> > sensorVisualisationFactories;
    MMM::MotionList motions;
    SensorVisualisationTable* sensorVisualisationTable;
};

typedef boost::shared_ptr<SensorVisualisationHandler> SensorVisualisationHandlerPtr;

#endif // __MMM_SENSORVISUALISATIONHANDLER_H_
