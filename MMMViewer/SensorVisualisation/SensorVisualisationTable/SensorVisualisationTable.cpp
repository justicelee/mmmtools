#include "SensorVisualisationTable.h"

#include "SensorVisualisationWidgetItem.h"

SensorVisualisationTable::SensorVisualisationTable() : QTreeWidget() {
    this->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    displayMessage("No motion loaded!");
}

void SensorVisualisationTable::displayMessage(const std::string &msg) {
    this->clear();
    this->setColumnCount(1);
    this->setHeaderLabels({QString::fromStdString(msg)});
}

void SensorVisualisationTable::populateTable(MMM::MotionList motions, std::map<std::string, std::map<std::string, MMM::SensorVisualisationPtr> > sensorVisualisations) {
    QList<QTreeWidgetItem*> items;
    for (MMM::MotionPtr motion : motions) {
        std::map<std::string, SensorVisualisationWidgetItem*> sensorByType;
        std::vector<SensorVisualisationWidgetItem*> sensorItemList;
        SensorVisualisationWidgetItem* motionItem(new SensorVisualisationWidgetItem(motion->getName()));
        for (auto const &sensorVis : sensorVisualisations[motion->getName()]) {
            SensorVisualisationWidgetItem* sensorTypeItem(new SensorVisualisationWidgetItem(sensorVis.first, sensorVis.second));
            if (sensorByType.find(sensorVis.second->getType()) == sensorByType.end()) {
                SensorVisualisationWidgetItem* sensorItem = new SensorVisualisationWidgetItem(sensorVis.second->getType() == "ModelPose" ? "Model" : sensorVis.second->getType(), sensorVis.second->getPriority());
                sensorByType[sensorVis.second->getType()] = sensorItem;
                sensorItemList.push_back(sensorItem);
            }
            sensorByType[sensorVis.second->getType()]->addChild(sensorTypeItem);
        }

        std::stable_sort(sensorItemList.begin(), sensorItemList.end(), [] (SensorVisualisationWidgetItem* p1, SensorVisualisationWidgetItem* p2) { return p1->getPriority() > p2->getPriority();});
        for (auto sensorItem : sensorItemList) motionItem->addChild(sensorItem);

        if (sensorByType.size() > 0) {
            for (auto const &sensor : sensorByType) {
                sensor.second->minimize();
            }
            QObject::connect(this, SIGNAL(itemChanged(QTreeWidgetItem*, int)), this, SLOT(changeVisualisation(QTreeWidgetItem*)));
            items.append(motionItem);
        } else {
            MMM_INFO << "Ignoring motion " << motion->getName() << ". Check needed sensor and sensor visualisation plugins!" << std::endl;
        }
    }

    if (items.size() > 0) {
        this->clear();
        this->setColumnCount(2);
        this->setHeaderLabels({QString("Visible"), QString("Visualisation")});
        this->insertTopLevelItems(0, items);
        this->expandAll();
        this->resizeColumnToContents(0);
        this->collapseAll();
    } else {
        MMM_ERROR << "Could not load motion visualisation. Check needed sensor and sensor visualisation plugins!" << std::endl;
    }
}

void SensorVisualisationTable::changeVisualisation(QTreeWidgetItem* svItem) {
    SensorVisualisationWidgetItem* item = static_cast<SensorVisualisationWidgetItem*>(svItem);
    item->changeVisualisation(svItem->checkState(0));
}
