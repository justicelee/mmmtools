#include "ModelPoseSensorVisualisationFactory.h"
#include "ModelPoseSensorVisualisation.h"

#include <boost/extension/extension.hpp>

#include <MMM/Motion/Plugin/ModelPosePlugin/ModelPoseSensor.h>

using namespace MMM;

// register this factory
SensorVisualisationFactory::SubClassRegistry ModelPoseSensorVisualisationFactory::registry(VIS_STR(ModelPoseSensor::TYPE), &ModelPoseSensorVisualisationFactory::createInstance);

ModelPoseSensorVisualisationFactory::ModelPoseSensorVisualisationFactory() : SensorVisualisationFactory() {}

ModelPoseSensorVisualisationFactory::~ModelPoseSensorVisualisationFactory() {}

std::string ModelPoseSensorVisualisationFactory::getName()
{
    return VIS_STR(ModelPoseSensor::TYPE);
}

SensorVisualisationPtr ModelPoseSensorVisualisationFactory::createSensorVisualisation(SensorPtr sensor, VirtualRobot::RobotPtr robot, boost::shared_ptr<VirtualRobot::CoinVisualization> visualization, SoSeparator* sceneSep) {
    ModelPoseSensorPtr s = boost::dynamic_pointer_cast<ModelPoseSensor>(sensor);
    if (!s) {
        MMM_ERROR << sensor->getType() << " could not be castet to " << ModelPoseSensor::TYPE << std::endl;
        return nullptr;
    }
    return SensorVisualisationPtr(new ModelPoseSensorVisualisation(s, robot, visualization, sceneSep));
}

SensorVisualisationFactoryPtr ModelPoseSensorVisualisationFactory::createInstance(void *)
{
    return SensorVisualisationFactoryPtr(new ModelPoseSensorVisualisationFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL SensorVisualisationFactoryPtr getFactory() {
    return SensorVisualisationFactoryPtr(new ModelPoseSensorVisualisationFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return SensorVisualisationFactory::VERSION;
}
