#include "EnvironmentalContactSensorVisualisationFactory.h"
#include "EnvironmentalContactSensorVisualisation.h"

#include <boost/extension/extension.hpp>

#include <MMM/Motion/Plugin/EnvironmentalContactPlugin/EnvironmentalContactSensor.h>

using namespace MMM;

// register this factory
SensorVisualisationFactory::SubClassRegistry EnvironmentalContactSensorVisualisationFactory::registry(VIS_STR(EnvironmentalContactSensor::TYPE), &EnvironmentalContactSensorVisualisationFactory::createInstance);

EnvironmentalContactSensorVisualisationFactory::EnvironmentalContactSensorVisualisationFactory() : SensorVisualisationFactory() {}

EnvironmentalContactSensorVisualisationFactory::~EnvironmentalContactSensorVisualisationFactory() {}

std::string EnvironmentalContactSensorVisualisationFactory::getName()
{
    return VIS_STR(EnvironmentalContactSensor::TYPE);
}

SensorVisualisationPtr EnvironmentalContactSensorVisualisationFactory::createSensorVisualisation(SensorPtr sensor, VirtualRobot::RobotPtr robot, boost::shared_ptr<VirtualRobot::CoinVisualization> visualization, SoSeparator* sceneSep) {
    EnvironmentalContactSensorPtr s = boost::dynamic_pointer_cast<EnvironmentalContactSensor>(sensor);
    if (!s) {
        MMM_ERROR << sensor->getType() << " could not be castet to " << EnvironmentalContactSensor::TYPE << std::endl;
        return nullptr;
    }
    return SensorVisualisationPtr(new EnvironmentalContactSensorVisualisation(s, robot, visualization, sceneSep));
}

SensorVisualisationFactoryPtr EnvironmentalContactSensorVisualisationFactory::createInstance(void *)
{
    return SensorVisualisationFactoryPtr(new EnvironmentalContactSensorVisualisationFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL SensorVisualisationFactoryPtr getFactory() {
    return SensorVisualisationFactoryPtr(new EnvironmentalContactSensorVisualisationFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return SensorVisualisationFactory::VERSION;
}

