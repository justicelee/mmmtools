#include "ImageSequenceExporterDialog.h"
#include "ui_ImageSequenceExporterDialog.h"
#include <boost/filesystem.hpp>
#include <QMessageBox>

ImageSequenceExporterDialog::ImageSequenceExporterDialog(const std::string &exportDirectoryPath, MMM::MotionList motions, QWidget* parent) :
    QDialog(parent),
    ui(new Ui::ImageSequenceExporterDialog),
    motions(motions),
    exportDirectoryPath(exportDirectoryPath)
{
    ui->setupUi(this);

    std::tuple<float, float> timestepTuple = MMM::Motion::calculateMinMaxTimesteps(motions);
    float minTimestep = std::get<0>(timestepTuple);
    float maxTimestep = std::get<1>(timestepTuple);

    ui->MinTimestepSpin->setMinimum(minTimestep);
    ui->MaxTimestepSpin->setValue(minTimestep);
    ui->MinTimestepSpin->setMaximum(maxTimestep);
    ui->MaxTimestepSpin->setMinimum(minTimestep);
    ui->MaxTimestepSpin->setValue(maxTimestep);
    ui->MaxTimestepSpin->setMaximum(maxTimestep);

    connect(ui->ExportButton, SIGNAL(clicked()), this, SLOT(exportMotions()));
    connect(ui->CancelButton, SIGNAL(clicked()), this, SLOT(close()));
}

void ImageSequenceExporterDialog::exportMotions() {
    float minTimestep = ui->MinTimestepSpin->value();
    float maxTimestep = ui->MaxTimestepSpin->value();
    int framesPerSecond = ui->FPSSpin->value();

    if (minTimestep > maxTimestep) {
        QMessageBox* msgBox = new QMessageBox(this);
        std::string error_message = "Minimum timestep should be smaller than maximum timestep!";
        msgBox->setText(QString::fromStdString(error_message));
        MMM_INFO << error_message << std::endl;
        msgBox->exec();
    }

    float rate = 1.0f / framesPerSecond;
    for (float timestep = minTimestep; timestep < maxTimestep; timestep += rate) {
        emit jumpTo(timestep);
        emit saveScreenshot(timestep, exportDirectoryPath);
    }
    close();
}

ImageSequenceExporterDialog::~ImageSequenceExporterDialog() {
    delete ui;
}
