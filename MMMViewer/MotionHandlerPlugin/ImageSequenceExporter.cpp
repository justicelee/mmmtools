#include "ImageSequenceExporter.h"
#include "ImageSequenceExporterDialog.h"
#include <QFileDialog>

using namespace MMM;

ImageSequenceExporter::ImageSequenceExporter(QWidget* widget) :
    MotionHandler(MotionHandlerType::EXPORT, "Export motion as image sequence"),
    searchPath(std::string(MMMTools_SRC_DIR)),
    widget(widget)
{
}

void ImageSequenceExporter::handleMotion(MotionList motions) {
    if (motions.size() == 0) return;

    std::string exportDirectoryPath = QFileDialog::getExistingDirectory(widget, QString::fromStdString(getDescription()), QString::fromStdString(searchPath)).toStdString();
    if (!exportDirectoryPath.empty()) {
        ImageSequenceExporterDialog* dialog = new ImageSequenceExporterDialog(exportDirectoryPath, motions, widget);
        connect(dialog, SIGNAL(jumpTo(float)), this, SIGNAL(jumpTo(float)));
        connect(dialog, SIGNAL(saveScreenshot(float,std::string,std::string)), this, SIGNAL(saveScreenshot(float,std::string,std::string)));
        dialog->show();
    }
}

std::string ImageSequenceExporter::getName() {
    return NAME;
}
