#!/bin/bash
if [ ${#2} -eq 0 ]
then
	# Argument 2 not present
	~/home/MMMTools/build/bin/MMMConverterGUI \
        --inputFile $1 \
        --converter "NloptConverter" \
        --converterConfigFile "ConverterVicon2MMM_WinterConfig.xml" \
        --targetModel "/MMMTools/data/Model/Winter/mmm.xml" \
        --targetModelProcessor "Winter" \
        --targetModelProcessorConfigFile "ModelProcessor_Winter.xml"
else
	# Argument 2 present
	~/home/MMMTools/build/bin/MMMConverter \
       --inputFile $1 \
        --converter "NloptConverter" \
        --converterConfigFile "ConverterVicon2MMM_WinterConfig.xml" \
        --targetModel "/MMMTools/data/Model/Winter/mmm.xml" \
        --targetModelProcessor "Winter" \
        --targetModelProcessorConfigFile "ModelProcessor_Winter.xml" \
		--outputFile $2
fi
