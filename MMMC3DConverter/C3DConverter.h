/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_C3DCONVERTER_H_
#define __MMM_C3DCONVERTER_H_

#include <MMM/Motion/Motion.h>
#include <MMM/Motion/MotionReaderC3D.h>

#include <tuple>

namespace MMM {

class C3DConverter
{
public:
    C3DConverter(const std::string &motionFilePath);

    MotionList convertMotions(const std::vector<std::tuple<std::string, std::string> > &motionNameToMarkerPrefix, const std::string &sensorName = std::string(), const std::string &sensorDescription = std::string());

    MMM::PrefixMarkerDataPtr getPrefixMarkerData();

private:
    MMM::PrefixMarkerDataPtr prefixMarkerData;
};

typedef boost::shared_ptr<C3DConverter> C3DConverterPtr;

}

#endif // __MMM_C3DCONVERTER_H_
