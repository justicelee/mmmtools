/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_C3DCONVERTERCONFIGURATION_H_
#define __MMM_C3DCONVERTERCONFIGURATION_H_

#include <boost/filesystem.hpp>
#include <string>
#include <VirtualRobot/RuntimeEnvironment.h>

#include "../common/ApplicationBaseConfiguration.h"

/*!
    Configuration of MMMC3DConverter.
*/
class MMMC3DConverterConfiguration : public ApplicationBaseConfiguration
{

public:
    std::string inputMotionPath;
    std::string outputMotionPath;
    std::vector<std::string> motionNames;
    std::string sensorName;
    std::string sensorDescription;
    std::vector<std::string> markerPrefix;

    MMMC3DConverterConfiguration() : ApplicationBaseConfiguration()
    {
    }

    bool processCommandLine(int argc, char *argv[])
    {
        VirtualRobot::RuntimeEnvironment::considerKey("inputMotion");
        VirtualRobot::RuntimeEnvironment::considerKey("outputMotion");
        VirtualRobot::RuntimeEnvironment::considerKey("motionName");
        VirtualRobot::RuntimeEnvironment::considerKey("sensorName");
        VirtualRobot::RuntimeEnvironment::considerKey("sensorDescription");
        VirtualRobot::RuntimeEnvironment::considerKey("markerPrefix");
        VirtualRobot::RuntimeEnvironment::processCommandLine(argc,argv);
        VirtualRobot::RuntimeEnvironment::print();

        inputMotionPath = getParameter("inputMotion", true, true);
        outputMotionPath = getParameter("outputMotion", false, false);
        if (outputMotionPath.empty())
            outputMotionPath = MMM::XML::getFileNameWithoutExtension(inputMotionPath) + ".xml";
        motionNames = getParameters("motionName", false);
        sensorName = getParameter("sensorName", false, false);
        sensorDescription = getParameter("sensorDescription", false, false);
        markerPrefix = getParameters("markerPrefix", true);
        if (markerPrefix.size() == 0) markerPrefix.push_back(std::string());

        if (motionNames.size() != markerPrefix.size()) {
            std::cout << "Motion names not matches marker prefixes!" << std::endl;
            valid = false;
        }

        return valid;
    }

    std::vector<std::string> getParameters(const std::string &parameterName, bool emptyAllowed)
    {
        std::vector<std::string> parameters;
        std::string parameterString = getParameter(parameterName);
        std::string splitStr = ";,";
        boost::split(parameters, parameterString, boost::is_any_of(splitStr));

        parameters.erase(std::unique(parameters.begin(), parameters.end()), parameters.end());
        if (!emptyAllowed) parameters.erase(std::remove_if(parameters.begin(), parameters.end(), std::mem_fun_ref(&std::string::empty)), parameters.end());

        return parameters;
    }

    void print()
    {
        MMM_INFO << "*** MMMC3DConverter Configuration ***" << std::endl;
        std::cout << "Input motion: " << inputMotionPath << std::endl;
        std::cout << "Output motion: " << outputMotionPath << std::endl;
    }
};


#endif
