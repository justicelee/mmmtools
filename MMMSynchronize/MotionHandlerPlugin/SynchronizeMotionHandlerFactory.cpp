#include "SynchronizeMotionHandlerFactory.h"
#include "SynchronizeMotionHandler.h"

#include <boost/extension/extension.hpp>

using namespace MMM;

// register this factory
MotionHandlerFactory::SubClassRegistry SynchronizeMotionHandlerFactory::registry(SynchronizeMotionHandler::NAME, &SynchronizeMotionHandlerFactory::createInstance);

SynchronizeMotionHandlerFactory::SynchronizeMotionHandlerFactory() : MotionHandlerFactory() {}

SynchronizeMotionHandlerFactory::~SynchronizeMotionHandlerFactory() {}

std::string SynchronizeMotionHandlerFactory::getName() {
    return SynchronizeMotionHandler::NAME;
}

MotionHandlerPtr SynchronizeMotionHandlerFactory::createMotionHandler(QWidget* widget) {
    return MotionHandlerPtr(new SynchronizeMotionHandler(widget));
}

MotionHandlerFactoryPtr SynchronizeMotionHandlerFactory::createInstance(void *) {
    return MotionHandlerFactoryPtr(new SynchronizeMotionHandlerFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL MotionHandlerFactoryPtr getFactory() {
    return MotionHandlerFactoryPtr(new SynchronizeMotionHandlerFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return MotionHandlerFactory::VERSION;
}
