#include <VirtualRobot/RuntimeEnvironment.h>
#include <MMM/MMMCore.h>
#include <MMM/XMLTools.h>

#include "MMMDataGloveConverterConfiguration.h"

MMMDataGloveConverterConfiguration::MMMDataGloveConverterConfiguration() : ApplicationBaseConfiguration()
{
}

bool MMMDataGloveConverterConfiguration::processCommandLine(int argc, char *argv[])
{
    VirtualRobot::RuntimeEnvironment::considerKey("mode");
    VirtualRobot::RuntimeEnvironment::considerKey("inputMotion");
    VirtualRobot::RuntimeEnvironment::considerKey("motionName");
    VirtualRobot::RuntimeEnvironment::considerKey("timestepDelta");
    VirtualRobot::RuntimeEnvironment::considerKey("sensorPlugins");

    VirtualRobot::RuntimeEnvironment::considerKey("data");
    VirtualRobot::RuntimeEnvironment::considerKey("config");

    VirtualRobot::RuntimeEnvironment::considerKey("outputMotion");

    VirtualRobot::RuntimeEnvironment::processCommandLine(argc,argv);

    testOnly = getParameter("mode") == "test";

    inputMotionPath = getParameter("inputMotion", !testOnly, true);
    motionName = getParameter("motionName");
    sensorPluginPaths = getParameters("sensorPlugins");
    std::string timestepDeltaStr = getParameter("timestepDelta");
    if (!timestepDeltaStr.empty()) timestepDelta = MMM::XML::convertToFloat(timestepDeltaStr.c_str());

    dataGloveDataPath = getParameter("data", true, true);
    dataGloveConfigPath = getParameter("config", true, true);

    outputMotionPath = getParameter("outputFile");
    if (outputMotionPath.empty()) outputMotionPath = "MMMDataGloveConverter_output.xml";

    return valid;
}

void MMMDataGloveConverterConfiguration::print()
{
    MMM_INFO << "*** MMMDataGlove Configuration ***" << std::endl;
    std::cout << "Input file: " << inputMotionPath << std::endl;
    std::cout << "Output file: " << outputMotionPath << std::endl;
}
