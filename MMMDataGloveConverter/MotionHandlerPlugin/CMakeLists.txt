project(AddDataGloveHandler)

set(AddDataGloveHandler_Sources
    AddDataGloveHandlerFactory.cpp
    AddDataGloveHandler.cpp
    AddDataGloveHandlerDialog.cpp
    ../DataGloveConverter.cpp
)

set(AddDataGloveHandler_Headers
    AddDataGloveHandlerFactory.h
    AddDataGloveHandler.h
    AddDataGloveHandlerDialog.h
    ../DataGloveConverter.h
)

set(AddDataGloveHandler_Moc
    AddDataGloveHandlerDialog.h
    AddDataGloveHandler.h
)

set(AddDataGloveHandler_Ui
    AddDataGloveHandlerDialog.ui
)

DefineMotionHandlerPlugin(${PROJECT_NAME} "${AddDataGloveHandler_Sources}" "${AddDataGloveHandler_Headers}" "${AddDataGloveHandler_Moc}" "${AddDataGloveHandler_Ui}" "")
