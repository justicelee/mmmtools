project(AddWholeBodyDynamicHandler)

set(AddWholeBodyDynamicHandler_Sources
    AddWholeBodyDynamicHandlerFactory.cpp
    AddWholeBodyDynamicHandler.cpp
    AddWholeBodyDynamicHandlerDialog.cpp
    ../WholeBodyDynamicCalculator.cpp
)

set(AddWholeBodyDynamicHandler_Headers
    AddWholeBodyDynamicHandlerFactory.h
    AddWholeBodyDynamicHandler.h
    AddWholeBodyDynamicHandlerDialog.h
    ../WholeBodyDynamicCalculator.h
)

set(AddWholeBodyDynamicHandler_Moc
    AddWholeBodyDynamicHandlerDialog.h
    AddWholeBodyDynamicHandler.h
)

set(AddWholeBodyDynamicHandler_Ui
    AddWholeBodyDynamicHandlerDialog.ui
)

DefineMotionHandlerPlugin(${PROJECT_NAME} "${AddWholeBodyDynamicHandler_Sources}" "${AddWholeBodyDynamicHandler_Headers}" "${AddWholeBodyDynamicHandler_Moc}" "${AddWholeBodyDynamicHandler_Ui}" "")
