/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_MOTIONCONVERTER_H_
#define __MMM_MOTIONCONVERTER_H_

#include <MMM/MMMCore.h>
#include <MMM/MMMImportExport.h>
#include <MMM/Model/Model.h>
#include <MMM/Motion/Motion.h>

#include <Eigen/Core>
#include <string> 
#include <vector>
#include <map>

namespace MMM
{

class MMM_IMPORT_EXPORT MotionConverter
{
public:
    MotionConverter(MotionPtr inputMotion, ModelPtr outputModel, ModelPtr outputModelUnprocessed, ModelProcessorPtr outputModelProcessor, const std::string &configFile, const std::string &convertedMotionName = std::string()) :
        inputMotion(inputMotion),
        outputModel(outputModel),
        outputModelUnprocessed(outputModelUnprocessed),
        outputModelProcessor(outputModelProcessor),
        configFile(configFile),
        convertedMotionName(convertedMotionName.empty() ? inputMotion->getName() : convertedMotionName)
    {
    }

    virtual void cancel() = 0;

    virtual float getCurrentTimestep() = 0;

	virtual MotionPtr convertMotion() = 0;

protected:

    MotionPtr inputMotion;
	ModelPtr outputModel;
    ModelPtr outputModelUnprocessed;
    ModelProcessorPtr outputModelProcessor;
    std::string configFile;
    std::string convertedMotionName;
    std::vector<std::string> joints;
};

typedef boost::shared_ptr<MotionConverter> MotionConverterPtr;

}

#endif 
