#include "ConvertMotionHandlerFactory.h"
#include "ConvertMotionHandler.h"

#include <boost/extension/extension.hpp>

using namespace MMM;

// register this factory
MotionHandlerFactory::SubClassRegistry ConvertMotionHandlerFactory::registry(ConvertMotionHandler::NAME, &ConvertMotionHandlerFactory::createInstance);

ConvertMotionHandlerFactory::ConvertMotionHandlerFactory() : MotionHandlerFactory() {}

ConvertMotionHandlerFactory::~ConvertMotionHandlerFactory() {}

std::string ConvertMotionHandlerFactory::getName() {
    return ConvertMotionHandler::NAME;
}

MotionHandlerPtr ConvertMotionHandlerFactory::createMotionHandler(QWidget* widget) {
    return MotionHandlerPtr(new ConvertMotionHandler(widget));
}

MotionHandlerFactoryPtr ConvertMotionHandlerFactory::createInstance(void *) {
    return MotionHandlerFactoryPtr(new ConvertMotionHandlerFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL MotionHandlerFactoryPtr getFactory() {
    return MotionHandlerFactoryPtr(new ConvertMotionHandlerFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return MotionHandlerFactory::VERSION;
}

