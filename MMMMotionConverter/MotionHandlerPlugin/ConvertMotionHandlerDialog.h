/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_CONVERTMOTIONHANDLERDIALOG_H_
#define __MMM_CONVERTMOTIONHANDLERDIALOG_H_

#ifndef Q_MOC_RUN
#include <MMM/Motion/Motion.h>
#include <MMM/Model/ModelProcessorWinter.h>
#include "../MotionConverterFactory.h"
#endif

#include <QDialog>
#include <QSettings>
#include <set>

namespace Ui {
class ConvertMotionHandlerDialog;
}

class ConvertMotionHandlerDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ConvertMotionHandlerDialog(QWidget* parent);

    ~ConvertMotionHandlerDialog();

    MMM::MotionPtr convertMotion(MMM::MotionList motions);

    virtual void reject();

    void update(std::map<std::string, MMM::MotionConverterFactoryPtr > motionConverterFactories);

private slots:
    void setCurrentMotion(int index);
    void loadTargetModel();
    void loadConverterConfiguration();
    void convertMotion();
    void setConvertedMotion(MMM::MotionPtr motion);

private:
    bool enableConvertMotionButton();
    void loadMotions(MMM::MotionList motions);
    void loadTargetModel(const std::string &modelFilePath, bool ignoreError = false);
    void loadConverterConfiguration(const std::string &converterConfigFilePath);

    MMM::ModelProcessorWinterPtr getModelProcessor();
    MMM::MotionConverterFactoryPtr getMotionConverterFactory();

    void setTargetModelLabel(const std::string &filePath);
    void setConverterConfigLabel(const std::string &filePath);
    void setConverterOptions();

    std::string getMotionName();

    MMM::MotionList motions;
    std::string motionName;
    MMM::MotionPtr currentMotion;
    MMM::ModelPtr targetModel;
    std::string converterConfigFilePath;
    std::map<std::string, MMM::MotionConverterFactoryPtr > motionConverterFactories;
    MMM::MotionPtr convertedMotion;

    QSettings settings;

    Ui::ConvertMotionHandlerDialog* ui;
};

#endif // __MMM_CONVERTMOTIONHANDLERDIALOG_H_
