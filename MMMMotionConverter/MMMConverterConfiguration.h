/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_CONVERTERCONFIGURATION_H_
#define __MMM_CONVERTERCONFIGURATION_H_

#include "../common/ApplicationBaseConfiguration.h"

/*!
    Configuration of command line converter.
    By default some standard parameters are set.
*/
class MMMConverterConfiguration : public ApplicationBaseConfiguration
{
public:
    MMMConverterConfiguration();

    virtual bool processCommandLine(int argc, char *argv[]);

    virtual void print();

    std::string inputMotionPath;
    std::string motionName;
    std::vector<std::string> sensorPluginPaths;

    std::vector<std::string> converterPluginPaths;
    std::string converterName;
    std::string converterConfigFilePath;

    std::string outputModelFilePath;
    std::string outputModelProcessor;
    std::string outputModelConfigFilePath;

    std::string outputMotionPath;

};

#endif
