/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef _MMM_NLOPTMOTIONCONVERTERFACTORY_h_
#define _MMM_NLOPTMOTIONCONVERTERFACTORY_h_

#include <MMM/MMMCore.h>
#include <boost/shared_ptr.hpp>
#include <string>

#include "../../MotionConverterFactory.h"

namespace MMM
{

/*!
    A factory to create a NloptMotionConverter converter.
*/
class MMM_IMPORT_EXPORT NloptMotionConverterFactory : public MotionConverterFactory
{
public:
    NloptMotionConverterFactory();
    virtual ~NloptMotionConverterFactory();

    MotionConverterPtr createMotionConverter(MotionPtr inputMotion, ModelPtr outputModel, ModelPtr outputModelUnprocessed, ModelProcessorPtr outputModelProcessor, const std::string &configFile, const std::string &convertedMotionName = std::string());

    std::string getName();

    static MotionConverterFactoryPtr createInstance(void*);

private:
    static SubClassRegistry registry;
};

typedef boost::shared_ptr<NloptMotionConverterFactory> NloptMotionConverterFactoryPtr;
}

#endif
