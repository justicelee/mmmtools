/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMMTools
* @author     Christian Mandery
* @copyright  2015 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#include <boost/extension/extension.hpp>

#include "NloptMotionConverterFactory.h"
#include "NloptMotionConverter.h"

namespace MMM
{

// register this factory
MotionConverterFactory::SubClassRegistry NloptMotionConverterFactory::registry(NloptMotionConverter::NAME, &NloptMotionConverterFactory::createInstance);

NloptMotionConverterFactory::NloptMotionConverterFactory(): MotionConverterFactory()
{
}

NloptMotionConverterFactory::~NloptMotionConverterFactory()
{
}

MotionConverterPtr NloptMotionConverterFactory::createMotionConverter(MotionPtr inputMotion, ModelPtr outputModel, ModelPtr outputModelUnprocessed, ModelProcessorPtr outputModelProcessor, const std::string &configFile, const std::string &convertedMotionName)
{
    return MotionConverterPtr(new NloptMotionConverter(inputMotion, outputModel, outputModelUnprocessed, outputModelProcessor, configFile, convertedMotionName));
}

std::string NloptMotionConverterFactory::getName()
{
    return NloptMotionConverter::NAME;
}

MotionConverterFactoryPtr NloptMotionConverterFactory::createInstance(void*)
{
    return boost::shared_ptr<MotionConverterFactory>(new NloptMotionConverterFactory());
}

}

extern "C"
BOOST_EXTENSION_EXPORT_DECL MMM::MotionConverterFactoryPtr getFactory() {
    MMM::NloptMotionConverterFactoryPtr f(new MMM::NloptMotionConverterFactory());
    return f;
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return MMM::MotionConverterFactory::VERSION;
}


