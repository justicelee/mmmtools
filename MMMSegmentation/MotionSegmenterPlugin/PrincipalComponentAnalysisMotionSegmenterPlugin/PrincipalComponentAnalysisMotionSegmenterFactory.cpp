#include "PrincipalComponentAnalysisMotionSegmenterFactory.h"
#include "PrincipalComponentAnalysisMotionSegmenter.h"

#include <boost/extension/extension.hpp>

using namespace MMM;

// register this factory
MotionSegmenterFactory::SubClassRegistry PrincipalComponentAnalysisMotionSegmenterFactory::registry(PrincipalComponentAnalysisMotionSegmenter::NAME, &PrincipalComponentAnalysisMotionSegmenterFactory::createInstance);

PrincipalComponentAnalysisMotionSegmenterFactory::PrincipalComponentAnalysisMotionSegmenterFactory() : MotionSegmenterFactory() {}

PrincipalComponentAnalysisMotionSegmenterFactory::~PrincipalComponentAnalysisMotionSegmenterFactory() {}

std::string PrincipalComponentAnalysisMotionSegmenterFactory::getName()
{
    return PrincipalComponentAnalysisMotionSegmenter::NAME;
}

MotionSegmenterPtr PrincipalComponentAnalysisMotionSegmenterFactory::createMotionSegmenter(MotionPtr motion, SegmentationType segType) {
    return MotionSegmenterPtr(new PrincipalComponentAnalysisMotionSegmenter(motion, segType));
}

MotionSegmenterFactoryPtr PrincipalComponentAnalysisMotionSegmenterFactory::createInstance(void *)
{
    return MotionSegmenterFactoryPtr(new PrincipalComponentAnalysisMotionSegmenterFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL MotionSegmenterFactoryPtr getFactory() {
    return MotionSegmenterFactoryPtr(new PrincipalComponentAnalysisMotionSegmenterFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return MotionSegmenterFactory::VERSION;
}

