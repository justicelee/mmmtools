#include "PrincipalComponentAnalysisMotionSegmenterConfiguration.h"

using namespace MMM;

PrincipalComponentAnalysisMotionSegmenterConfiguration::PrincipalComponentAnalysisMotionSegmenterConfiguration(float alpha, int reducedDimensions, KeyframeType keyframeType, const std::set<std::string> &names) :
    alpha(alpha),
    reducedDimensions(reducedDimensions),
    keyframeType(keyframeType),
    segmentNames(names)
{
}

