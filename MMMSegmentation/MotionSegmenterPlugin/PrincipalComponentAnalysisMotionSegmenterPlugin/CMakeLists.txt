project(PrincipalComponentAnalysisMotionSegmenter)

set(PrincipalComponentAnalysisMotionSegmenter_Sources
    PrincipalComponentAnalysisMotionSegmenterFactory.cpp
    PrincipalComponentAnalysisMotionSegmenter.cpp
    PrincipalComponentAnalysisMotionSegmenterWidget.cpp
    PrincipalComponentAnalysisMotionSegmenterConfiguration.cpp
    ../../Segmentation.cpp
)

set(PrincipalComponentAnalysisMotionSegmenter_Headers
    PrincipalComponentAnalysisMotionSegmenterFactory.h
    PrincipalComponentAnalysisMotionSegmenter.h
    PrincipalComponentAnalysisMotionSegmenterWidget.h
    PrincipalComponentAnalysisMotionSegmenterConfiguration.h
    ../../Segmentation.h
)

set(PrincipalComponentAnalysisMotionSegmenter_Moc
    PrincipalComponentAnalysisMotionSegmenterWidget.h
)

set(PrincipalComponentAnalysisMotionSegmenter_Ui
    PrincipalComponentAnalysisMotionSegmenterWidget.ui
)

set(PrincipalComponentAnalysisMotionSegmenter_Sensors KinematicSensor ModelPoseSensor MoCapMarkerSensor)

DefineMotionSegmenterPlugin(${PROJECT_NAME} "${PrincipalComponentAnalysisMotionSegmenter_Sources}" "${PrincipalComponentAnalysisMotionSegmenter_Headers}" "${PrincipalComponentAnalysisMotionSegmenter_Moc}" "${PrincipalComponentAnalysisMotionSegmenter_Ui}" "${PrincipalComponentAnalysisMotionSegmenter_Sensors}")
