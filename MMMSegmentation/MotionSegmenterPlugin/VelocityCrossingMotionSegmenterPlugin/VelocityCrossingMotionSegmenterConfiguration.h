/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/


#ifndef __MMM_VELOCITYCROSSINGMOTIONSEGMENTERCONFIGURATION_H_
#define __MMM_VELOCITYCROSSINGMOTIONSEGMENTERCONFIGURATION_H_

#include "../../MotionSegmenter.h"
#include <set>

namespace MMM
{

class VelocityCrossingMotionSegmenterConfiguration
{
public:
    VelocityCrossingMotionSegmenterConfiguration(float velocityThreshold, int segmentThreshold, KeyframeType keyframeType, const std::set<std::string> &names);
    
    float velocityThreshold;
    int segmentThreshold;
    KeyframeType keyframeType;
    std::set<std::string> segmentNames;
};

typedef boost::shared_ptr<VelocityCrossingMotionSegmenterConfiguration> VelocityCrossingMotionSegmenterConfigurationPtr;

}
#endif // __MMM_VELOCITYCROSSINGMOTIONSEGMENTERCONFIGURATION_H_
