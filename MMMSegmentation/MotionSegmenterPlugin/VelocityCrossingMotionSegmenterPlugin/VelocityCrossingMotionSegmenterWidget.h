/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_VELOCITYCROSSINGMOTIONSEGMENTERWIDGET_H_
#define __MMM_VELOCITYCROSSINGMOTIONSEGMENTERWIDGET_H_

#ifndef Q_MOC_RUN
#include "VelocityCrossingMotionSegmenterConfiguration.h"
#include <MMM/Motion/Motion.h>
#include <QWidget>
#endif

namespace Ui {

class VelocityCrossingMotionSegmenterWidget;
}

class VelocityCrossingMotionSegmenterWidget : public QWidget
{
    Q_OBJECT

public:
    explicit VelocityCrossingMotionSegmenterWidget(MMM::MotionPtr motion, MMM::SegmentationType segType, QWidget* parent = 0);
    ~VelocityCrossingMotionSegmenterWidget();

    MMM::VelocityCrossingMotionSegmenterConfigurationPtr getConfiguration();

    void setSegmentationType(MMM::SegmentationType &type);
    
    void setMotion(MMM::MotionPtr motion);

private:
    void updateTreeWidget();
    
    Ui::VelocityCrossingMotionSegmenterWidget* ui;
    MMM::SegmentationType segType;
    MMM::MotionPtr motion;
};

#endif // __MMM_VELOCITYCROSSINGMOTIONSEGMENTERWIDGET_H_
