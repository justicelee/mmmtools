#include "SegmentMotionHandlerFactory.h"
#include "SegmentMotionHandler.h"

#include <boost/extension/extension.hpp>

using namespace MMM;

// register this factory
MotionHandlerFactory::SubClassRegistry SegmentMotionHandlerFactory::registry(SegmentMotionHandler::NAME, &SegmentMotionHandlerFactory::createInstance);

SegmentMotionHandlerFactory::SegmentMotionHandlerFactory() : MotionHandlerFactory() {}

SegmentMotionHandlerFactory::~SegmentMotionHandlerFactory() {}

std::string SegmentMotionHandlerFactory::getName() {
    return SegmentMotionHandler::NAME;
}

MotionHandlerPtr SegmentMotionHandlerFactory::createMotionHandler(QWidget* widget) {
    return MotionHandlerPtr(new SegmentMotionHandler(widget));
}

MotionHandlerFactoryPtr SegmentMotionHandlerFactory::createInstance(void *) {
    return MotionHandlerFactoryPtr(new SegmentMotionHandlerFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL MotionHandlerFactoryPtr getFactory() {
    return MotionHandlerFactoryPtr(new SegmentMotionHandlerFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return MotionHandlerFactory::VERSION;
}
