/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_JOINMOTIONSCONFIGURATION_H_
#define __MMM_JOINMOTIONSCONFIGURATION_H_

#include <boost/filesystem.hpp>
#include <string>
#include <VirtualRobot/RuntimeEnvironment.h>

#include "../common/ApplicationBaseConfiguration.h"

/*!
    Configuration of MMMJoinMotions.
*/
class MMMJoinMotionsConfiguration : public ApplicationBaseConfiguration
{
public:
    MMMJoinMotionsConfiguration() : ApplicationBaseConfiguration()
    {
    }

    bool processCommandLine(int argc, char *argv[])
    {
        VirtualRobot::RuntimeEnvironment::considerKey("inputMotion1");
        VirtualRobot::RuntimeEnvironment::considerKey("inputMotion2");
        VirtualRobot::RuntimeEnvironment::considerKey("motionName1");
        VirtualRobot::RuntimeEnvironment::considerKey("motionName2");
        VirtualRobot::RuntimeEnvironment::considerKey("sensorPlugins");
        VirtualRobot::RuntimeEnvironment::considerKey("outputMotion");
        VirtualRobot::RuntimeEnvironment::processCommandLine(argc,argv);
        VirtualRobot::RuntimeEnvironment::print();

        inputMotionPath1 = getParameter("inputMotion1", true, true);
        inputMotionPath2 = getParameter("inputMotion2", true, true);
        motionName1 = getParameter("motionName1", false, false);
        motionName2 = getParameter("motionName2", false, false);
        sensorPluginPaths = getParameters("sensorPlugins");
        outputMotionPath = getParameter("outputMotion", false, false);
        if (outputMotionPath.empty()) outputMotionPath = inputMotionPath1;

        return valid;
    }

    void print()
    {
        MMM_INFO << "*** MMMJoinMotions Configuration ***" << std::endl;
        std::cout << "Input motion number 1: " << inputMotionPath1 << std::endl;
        std::cout << "Input motion number 2: " << inputMotionPath2 << std::endl;
        std::cout << "Motion name number 1: " << motionName1 << std::endl;
        std::cout << "Motion name number 2: " << motionName2 << std::endl;
        std::cout << "Output motion: " << outputMotionPath << std::endl;
    }

    std::string inputMotionPath1;
    std::string inputMotionPath2;
    std::string motionName1;
    std::string motionName2;
    std::vector<std::string> sensorPluginPaths;
    std::string outputMotionPath;
};


#endif // __MMM_JOINMOTIONSCONFIGURATION_H_
