#include "MotionListConfiguration.h"

using namespace MMM;

MotionListConfiguration::MotionListConfiguration(int index, int newIndex, const std::string &motionName, bool ignoreMotion) :
    index(index),
    newIndex(newIndex),
    motionName(motionName),
    ignoreMotion(ignoreMotion)
{
}

int MotionListConfiguration::getIndex() {
    return index;
}

int MotionListConfiguration::getNewIndex() {
    return newIndex;
}

std::string MotionListConfiguration::getMotionName() {
    return motionName;
}

bool MotionListConfiguration::isIgnoreMotion() {
    return ignoreMotion;
}

bool SortByNewIndexFunctor::operator()(const MMM::MotionListConfigurationPtr& configuration1, const MMM::MotionListConfigurationPtr& configuration2) {
    return(configuration1->getNewIndex() < configuration2->getNewIndex());
}
