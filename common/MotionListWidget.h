/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_MOTIONLISTIWIDGET_H_
#define __MMM_MOTIONLISTIWIDGET_H_

#include "MotionListConfiguration.h"
#include <QListWidget>

class MotionListWidget : public QListWidget
{
    Q_OBJECT

public:
    MotionListWidget(const std::vector<std::string> &motionNames, const std::string &motionToolTipFirstPart = "Init motion name: ");

    /*! @throws MMMException if names are empty or doubled */
    std::vector<MMM::MotionListConfigurationPtr> getConfigurations();

private:
    void addMotionNames(const std::vector<std::string> &motionNames);

    void enableDragDrop();

    MMM::MotionListConfigurationPtr getConfiguration(int index);

    std::string motionToolTipFirstPart;
};

#endif // __MMM_MOTIONLISTIWIDGET_H_
